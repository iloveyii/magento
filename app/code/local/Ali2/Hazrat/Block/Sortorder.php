<?php

class Ali_Hazrat_Block_Sortorder extends Mage_Core_Block_Template {
    /**
    * prepare block's layout
    *
    * @return Magestore_Lesson13_Block_Lesson13
    */
    public function construct() {
        parent:: construct();
        $collection = Mage::getModel('hazrat/product')->getCollection();
        $this->setCollection($collection);
    }
}
