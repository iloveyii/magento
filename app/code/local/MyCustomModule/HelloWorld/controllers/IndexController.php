<?php

class MyCustomModule_HelloWorld_IndexController extends Mage_Core_Controller_Front_Action {
    
    public function indexAction() {
       
        $arrSortOrder = [
            'ASC'=>'DESC' ,
            'DESC'=>'ASC'
        ];
        
        $arrArrows = [
            'ASC'=>'&darr;' ,
            'DESC'=>'&uarr;'
        ];
        // Initialize Mage_Core_Model_Layout model
        $this->loadLayout();
        
        $sortorder= $this->getRequest()->getParam('sortorder');
        
        $breadcrumbs = $this->getLayout()->getBlock("breadcrumbs");
        $breadcrumbs->addCrumb("titlename", array(
                "label" => $this->__("Sort Order " . $arrArrows[$sortorder]),
                "title" => $this->__("Sort Order"),
                "link"  => Mage::getUrl('helloworld/index/index', ['sortorder'=>$arrSortOrder[$sortorder]]) 
		   ));
        // Build the page as per configuration
        $this->renderLayout();
    }
    
    
}
